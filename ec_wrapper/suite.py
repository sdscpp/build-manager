
import ecflow


class Suite(ecflow.Suite):
    def __init__(self, name, variables=None, tasks=None, families=None):
        super(Suite, self).__init__(name)
        for k, v in (variables or {}).iteritems():
            print "variable: ", k, v
            self.add_variable(k, v)
        for t in tasks or []:
            self.add_task(t)
        for f in families or []:
            self.add_family(f)