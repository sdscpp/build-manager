
import ecflow


class Defs(ecflow.Defs):

    def __init__(self, suites=None):
        """
        Initialise Definitions
        :param suites: A list of ecflow.Suite or ec_wrapper.Suite objects.
        """
        super(Defs, self).__init__()
        for s in suites or []:
            self.add_suite(s)
