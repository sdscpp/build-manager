import os

import ecflow


class Client(ecflow.Client):
    def __init__(self, host='localhost', port=3141, name=None, password=None, try_no=None):
        super(Client, self).__init__(host, port)
        self.set_host_port(host, int(port))
        self.host = host
        self.port = port

        self.set_child_pid(os.getpid())
        if name is not None:
            self.set_child_path(name)
        if password is not None:
            self.set_child_password(password)
        if try_no is not None:
            self.set_child_try_no(try_no)


    def __unicode__(self):
        return "ecFlow client {}:{}".format(self.host, self.port)
