#!/usr/bin/env python2.7

# This suite exists only to test some stuff

import ec_wrapper
import os

import sys
print sys.path

assert __name__ == '__main__', "This is intended as (only) a testing executable"

def generate():

    return ec_wrapper.Defs(
        suites=[
            ec_wrapper.Suite(
                'test',
                variables={
                    'ECF_HOME': os.path.dirname(__file__),
                    'ECF_INCLUDE': os.path.join(os.path.dirname(__file__), 'include')
                },
                tasks=[ec_wrapper.Task('t1')]
            )
        ]
    )

d = generate()

print d

c = ec_wrapper.Client()
# c.load(d)
# TODO: Test current status first, and load/begin or suspend/replace/resume as appropriate
c.suspend('/test')
c.replace('/test', d)
c.resume('/test')

print c


