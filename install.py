#!/usr/bin/env python2.7

import sys
import os
import errno

import ec_wrapper
from repo import ECFRepo


def link_if_not_exist(src_dir, tgt_dir, filename):
    try:
        os.symlink(os.path.join(src_dir, filename), os.path.join(tgt_dir, filename))
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


print "Inside the build manager installation"

assert len(sys.argv) >= 3

# Repo that this install script is in
repo = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))

# Where should dependent repos be cloned to, and
git_root = sys.argv[1]
ecf_root = sys.argv[2]

print "Build manager repo: {}".format(repo)
print "Git root for children: {}".format(git_root)
print "ecFlow suite root: {}".format(ecf_root)

# Generate the manager wrapper

defs = ec_wrapper.Defs(
    suites=[
        ec_wrapper.Suite(
            'build_manager',
            variables={
                'BUILD_MANAGER_REPO': repo,
                'GIT_ROOT': git_root,
                'SUITES_ROOT': ecf_root,

                'ECF_HOME': ecf_root,
                'ECF_INCLUDE': os.path.join(repo, 'include')
            },
            tasks=[
                ec_wrapper.Task('update_repo'),
                ec_wrapper.Task('refresh_child_repos')
            ]
        )
    ]
)

# Generate the required ecf files

bm_dir = os.path.join(ecf_root, 'build_manager')
try:
    os.makedirs(bm_dir)
except os.error:
    pass

link_if_not_exist(os.path.join(repo, 'ecf_scripts'), bm_dir, 'update_repo.ecf')
link_if_not_exist(os.path.join(repo, 'ecf_scripts'), bm_dir, 'refresh_child_repos.ecf')

# Install it!!!

c = ec_wrapper.Client()
c.delete_all()
c.load(defs)
c.begin_suite('build_manager')

# Clone the managed suites

try:
    os.makedirs(git_root)
except os.error:
    pass



for repo in sys.argv[3:]:

    r = ECFRepo(repo, git_root=git_root)
    r.update()

# And ensure that the server is running

c.sync_local()
if c.get_defs().get_server_state() == ec_wrapper.SState.RUNNING:
    c.restart_server()
