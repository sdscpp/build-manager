"""
The machinery to automatically update, and refresh the installation of a given repo.

This includes the special case code for self-updating the build_manager repo (which controls everything else...)
"""
import os
from contextlib import closing

from dulwich import config as git_config
from dulwich import porcelain
from dulwich import repo as git_repo


class ECFRepo(object):

    def __init__(self, remote_path=None, local_path=None, git_root=None):

        # Get the name from the remote path

        if remote_path is None:
            assert local_path is not None
            c = git_config.ConfigFile.from_path(os.path.join(local_path, '.git/config'))
            remote_path = c[('remote', 'origin')]['url']

        name, extension = os.path.splitext(os.path.basename(remote_path))
        if extension != '.git':
            name = os.path.basename(remote_path)

        # If we haven't provided the local path, then guess it!

        if local_path is not None:
            name = os.path.basename(local_path)

        # TODO: Get remote path from existing repo
        assert remote_path is not None

        if local_path is None:
            local_path = os.path.join(git_root, name)

        self.remote_path = remote_path
        self.local_path = local_path

    def repo(self):
        return closing(git_repo.Repo(self.local_path))

    def fetch(self):
        """
        Fetch from remotes
        """
        with self.repo() as r:
            print "Fetching {} into {}".format(self.remote_path, self.local_path)
            porcelain.fetch(r, self.remote_path)

    def clone(self):
        """
        Clone the repository!!!
        """
        print "Cloning {} into {}".format(self.remote_path, self.local_path)
        porcelain.clone(self.remote_path, target=self.local_path).close()

    def update(self):
        if os.path.exists(os.path.join(self.local_path, '.git/config')):
            self.fetch()
        else:
            self.clone()

        with self.repo() as r:
            print "Switching to latest HEAD"
            tree = r['HEAD'].tree
            r.reset_index(tree)
