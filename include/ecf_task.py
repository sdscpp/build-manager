
import sys
import os

import signal

global ecflow_variables
ecflow_variables = {
    'ECF_INCLUDE': "%ECF_INCLUDE%",
    'ECF_NODE': "%ECF_NODE%",
    'ECF_PORT': "%ECF_PORT%",
    'ECF_NAME': "%ECF_NAME%",
    'ECF_PASS': "%ECF_PASS%",
    'ECF_TRYNO': "%ECF_TRYNO%"
}

sys.path.append(os.path.dirname(ecflow_variables['ECF_INCLUDE']))
from ec_wrapper import client

# Wrap the ecflow client in a way that works well for tasks

class Client(client.Client):

    def __init__(self):

        host = ecflow_variables['ECF_NODE']
        port = ecflow_variables['ECF_PORT']
        name = ecflow_variables['ECF_NAME']
        password = ecflow_variables['ECF_PASS']
        try_no = int(ecflow_variables['ECF_TRYNO'])

        super(Client, self).__init__(
            host=host,
            port=port,
            name=name,
            password=password,
            try_no=try_no
        )

        # Only wait 20 seconds before failing if the server cannot be found (default is 24 hours)
        self.set_child_timeout(20)

    def __enter__(self):

        # Abort on the following signals
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGHUP, self.signal_handler)
        signal.signal(signal.SIGQUIT, self.signal_handler)
        signal.signal(signal.SIGILL, self.signal_handler)
        signal.signal(signal.SIGTRAP, self.signal_handler)
        signal.signal(signal.SIGIOT, self.signal_handler)
        signal.signal(signal.SIGBUS, self.signal_handler)
        signal.signal(signal.SIGFPE, self.signal_handler)
        signal.signal(signal.SIGUSR1, self.signal_handler)
        signal.signal(signal.SIGUSR2, self.signal_handler)
        signal.signal(signal.SIGPIPE, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGXCPU, self.signal_handler)
        signal.signal(signal.SIGPWR, self.signal_handler)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type != None:
            self.child_abort("Aborted with exception type: {} : {}".format(exc_type, exc_val))
            return False
        self.child_complete()
        return False

    def signal_handler(self, signum, frame):
        print "Signal handler called with signal: {}".format(signum)
        self.child_abort("Signal handler called with signal: {}".format(signum))
